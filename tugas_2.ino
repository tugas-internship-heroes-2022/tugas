// JIHAN AANISA ZULFAANI
// 21/480765/PA/20904

// deklarasi pin yang digunakan
const int pintrig = 7;
const int pinecho = 6;
const int pinmtr1 = 4;
const int pinmtr2 = 3;

void setup() {
  Serial.begin(9600);
  pinMode(pintrig, OUTPUT);
  pinMode(pinecho, INPUT);
  pinMode(pinmtr1, OUTPUT);
  pinMode(pinmtr2, OUTPUT);
}

long durasi = 0;
void loop() {
  digitalWrite(pinmtr1, HIGH);
  delay(1000);
  analogWrite(pinmtr2, 127);
  delay(1000);
  
  digitalWrite(pintrig, HIGH);
  delay(10); 
  digitalWrite(pintrig, LOW);
  
  durasi = pulseIn(pinecho, HIGH);
  Serial.print("Waktu: ");
  Serial.print(durasi);
  
  float jarak = (durasi*0.034)/2;
  Serial.print(", Jarak: "); 
  Serial.print(jarak);
  Serial.print(" cm");
  Serial.print(", "); 
  Serial.print(jarak*0.393701);
  Serial.println(" inch");
  delay(1000);
}
