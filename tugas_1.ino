// JIHAN AANISA ZULFAANI
// 21/480765/PA/20904

// deklarasi pin yang digunakan
#define pinmtr1 4
#define pinmtr2 3
#define pinmtr3 7
#define pinmtr4 6

void setup() {
// karena motor dc digunakan sebagai keluaran, maka diberi OUTPUT
// untuk inputnya di motor driver
  pinMode(pinmtr1, OUTPUT);
  pinMode(pinmtr2, OUTPUT);
  pinMode(pinmtr3, OUTPUT);
  pinMode(pinmtr4, OUTPUT);
}

void loop() {
// karena pinmtr1 dan pinmtr3 digital, maka diberikan HIGH/LOW
// untuk mendapatkan motor bergerak setengah dari max rpm maka
// pada kedua analog pinmtr2 dan pinmtr4 saya beri nilai 120
  digitalWrite(pinmtr1, HIGH);
  analogWrite(pinmtr2, 120);
  digitalWrite(pinmtr3, LOW);
  analogWrite(pinmtr4, 120);
}
